#!/bin/bash
###############################################################################
# @file build-app.sh
#
# bash script to manage build of alarm-webrtc-streamer
#
# @author David Park <dpark@alarm.com>
# @date 2020-01-02
#
###############################################################################
#
# Alarm.com Copyright 2000-2021
#
# COPYRIGHT STATEMENT.
# This software has been provided pursuant to a License Agreement containing
# restrictions on its use.  The software contains confidential information,
# trade secrets, patents (pending or approved) and proprietary information of
# Alarm.com and is protected by Federal copyright law.  This
# confidential information or source code files, should not be copied or distributed,
# in any form or medium, disclosed to any third party, or taken outside
# Alarm.com or authorized offices or used in any manner, except with prior
# written agreement with Alarm.com . All rights reserved.
#
# Alarm.com
#
###############################################################################
set -e
set -u
set -o pipefail

PROG_NAME=$0
ROOTDIR=$PWD/../
ADC_MODELS=(x23 V622 x86_64)
UPGRADE_ITEMS=(gst-sdk, toolchain)
MODEL_SELECTION=${ADC_MODELS[0]}
TOOLCHAINS_DIR=/opt/toolchains/webrtc
TOOLCHAINS_SOURCE=https://artifactory.corp.adcinternal.com/artifactory/list/webrtc/toolchains/streamer
SUDO_CMD=sudo
if [[ "$(whoami)" == 'bamboo' ]]; then
    SUDO_CMD='' # builder user cannot run sudo
fi
DISTRO=$(lsb_release -i | awk '{print $NF}')

info() {
    echo "$(basename $0) [INFO]: $1"
}

warn() {
    echo "$(basename $0) [WARN]: $1"
}

error() {
    echo "$(basename $0) [ERROR]: $1"
    exit 1
}

usage () {
    echo "usage: $PROG_NAME [-c] [-h] [-m MODEL] [-u]"
    echo "options:"
    echo "    -c perform a clean build"
    echo "    -h help, displays usage"
    echo "    -m MODEL specify model"
    echo "        choices: ${ADC_MODELS[*]} (default is ${ADC_MODELS[0]})"
    echo "    -u upgrade item"
    echo "        choices: ${UPGRADE_ITEMS[*]}"
    echo ""
    echo "example:"
    echo "    To build alarm-webrtc-streamer for ${MODEL_SELECTION} models"
    echo "    $PROG_NAME -c -m ${MODEL_SELECTION}"
}

valid_model () {
    local model_list="$1[@]"
    local model=$2
    local valid=1
    for element in "${!model_list}"; do
        if [[ $element == "$model" ]]; then
            valid=0
            break
        fi
    done
    return $valid
}

program_exists() {
    command -v "$1" >/dev/null
}

directory_exists() {
    if [ -d "$1" ]; then 
        true
    else
        false
    fi
}

directory_empty() {
    if directory_exists $1; then
        if [ ! "$(ls -A $1)" ]; then
            true
        else
            false
        fi
    else
        true
    fi
}

toolchain_file() {
    case $1 in
        V622)
            echo "linaro-s2e"
            ;;
        x23)
            echo "linaro-s5l"
            ;;
        ?)
            echo ""
            ;;
    esac
}

install_toolchain() {
    case $1 in
        V622)
            DESTDIR=${TOOLCHAINS_DIR}/$1
            ${SUDO_CMD} mkdir -p $DESTDIR
            local toolchain_filename=$(toolchain_file $1)
            ${SUDO_CMD} wget -c ${TOOLCHAINS_SOURCE}/${toolchain_filename}.tgz -O - | tar -xzv -C $DESTDIR
            ;;
        x23)
            DESTDIR=${TOOLCHAINS_DIR}/$1
            ${SUDO_CMD} mkdir -p $DESTDIR
            local toolchain_filename=$(toolchain_file $1)
            ${SUDO_CMD} wget -c ${TOOLCHAINS_SOURCE}/${toolchain_filename}.tgz -O - | tar -xzv -C $DESTDIR
            rm -rf 
            ;;
        x86_64)
            DESTDIR=${TOOLCHAINS_DIR}/$1
            ${SUDO_CMD} mkdir -p $DESTDIR
            ;;
        ?)
            error "unrecognized model, failed to install toolchain"
            exit 1
            ;;
    esac
}

install_dependencies_if_necessary() {
    if [[ "$DISTRO" == "CentOS" ]]; then
        return 0
    fi

    if ! program_exists pkg-config; then
        info "pkg-config NOT found, installing"
        ${SUDO_CMD} apt update
        ${SUDO_CMD} apt install -y pkg-config
    fi

    if ! program_exists pip3; then
        info "pip3 NOT found, installing"
        ${SUDO_CMD} apt install -y python3-pip
    fi

    if ! program_exists meson; then
        info "meson NOT found, installing"
        ${SUDO_CMD} python3 -m pip install meson
    fi

    if ! program_exists ninja; then
        info "ninja not found, installing"
        ${SUDO_CMD} apt install -y ninja-build
    fi

    if ! program_exists wget; then
        info "wget not found, installing"
        ${SUDO_CMD} apt install -y wget
    fi
}

install_gst_sdk() {

    if valid_model ADC_MODELS $1; then
        local SDK_PACKAGE="gstreamer-1.0-$1.tar.bz2"
        local DESTDIR=${TOOLCHAINS_DIR}/$1/gst-sdk

        ${SUDO_CMD} mkdir -p $DESTDIR
        curl ${TOOLCHAINS_SOURCE}/$SDK_PACKAGE -o $ROOTDIR/gst-sdk/$SDK_PACKAGE
        ${SUDO_CMD} wget ${TOOLCHAINS_SOURCE}/$SDK_PACKAGE -O - | tar -jx -C $DESTDIR
        info "installed gst sdk for $1"
    else
        error "unrecognized model"    
    fi
}

upgrade() {
    case $1 in
        gst-sdk)
            ${SUDO_CMD} rm -rf ${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk
            info "upgrading gst-sdk for model $MODEL_SELECTION"
            install_gst_sdk $MODEL_SELECTION
            ;;
        toolchain)
            ${SUDO_CMD} rm -rf ${TOOLCHAINS_DIR}/$MODEL_SELECTION
            info "upgrading toolchain for model $MODEL_SELECTION"
            install_toolchain $MODEL_SELECTION
            ;;
        ?)
            error "unknown upgrade - choices are: ${UPGRADE_ITEMS[*]}"
            ;;
    esac
}

while getopts 'chm:u:' OPTION; do
    case "$OPTION" in
        c)
            rm -rf build install
            ;;
        h)
            usage
            exit 1
            ;;
        m)
            model="$OPTARG"
            if valid_model ADC_MODELS "$model"; then
                MODEL_SELECTION=$model
            else
                error "invalid model selection"
            fi
            ;;
        u)
            upgrade $OPTARG
            exit 1
            ;;
        ?)
            usage
            exit 1
            ;;
    esac
done
shift "$(($OPTIND -1))"

info "model selection is $MODEL_SELECTION"

export GST_SDK_PATH=${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk

if ! directory_exists ${TOOLCHAINS_DIR}/$MODEL_SELECTION; then
    ${SUDO_CMD} mkdir ${TOOLCHAINS_DIR}/$MODEL_SELECTION
fi

TOOLCHAIN_DIR=$(toolchain_file ${MODEL_SELECTION})

if ! directory_exists ${TOOLCHAINS_DIR}/$MODEL_SELECTION/$TOOLCHAIN_DIR; then
    install_toolchain $MODEL_SELECTION
else
    info "${TOOLCHAINS_DIR}/$MODEL_SELECTION/$TOOLCHAIN_DIR exists"
fi

install_dependencies_if_necessary

if directory_empty ${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk; then
    install_gst_sdk $MODEL_SELECTION
fi

MESON_EXEC=$(command -v meson)
if [[ "$DISTRO" == "CentOS" ]]; then
    NINJA_EXEC=$(command -v ninja-build)
else
    NINJA_EXEC=$(command -v ninja)
fi

info "meson executable located at $MESON_EXEC"
info "ninja executable located at $NINJA_EXEC"

if [[ "$MODEL_SELECTION" == "x86_64" ]]; then
    export LD_LIBRARY_PATH=${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk/lib/gstreamer-1.0${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
    export LD_LIBRARY_PATH=${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk/lib/gio/modules${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
    export LD_LIBRARY_PATH=${TOOLCHAINS_DIR}/$MODEL_SELECTION/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
    export LD_LIBRARY_PATH=${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
    export GST_PLUGIN_PATH=${TOOLCHAINS_DIR}/$MODEL_SELECTION/gst-sdk/lib/gstreamer-1.0
    info "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
    info "GST_PLUGIN_PATH=$GST_PLUGIN_PATH"
fi

$MESON_EXEC --cross-file data/cross-file/adc-model-$MODEL_SELECTION.txt --prefix=$(pwd)/install build --strip
$NINJA_EXEC -C build/ install
