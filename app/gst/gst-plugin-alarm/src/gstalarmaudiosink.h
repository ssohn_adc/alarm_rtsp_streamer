/*
 * Copyright © 2019 VXG Inc., Alarm.com. All rights reserved.
 *
 * Author: Ilya Smelykh <ilya@videoexpertsgroup.com>
 */

#ifndef __GST_ALARM_AUDIO_SINK_H__
#define __GST_ALARM_AUDIO_SINK_H__

#include <gst/gst.h>
#include "gstalarmipc.h"

G_BEGIN_DECLS

#define GST_TYPE_ALARM_AUDIO_SINK            (gst_alarm_audio_sink_get_type())
#define GST_ALARM_AUDIO_SINK(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ALARM_AUDIO_SINK,GstAlarmAudioSink))
#define GST_ALARM_AUDIO_SINK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ALARM_AUDIO_SINK,GstAlarmAudioSinkClass))
#define GST_IS_ALARM_AUDIO_SINK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ALARM_AUDIO_SINK))
#define GST_IS_ALARM_AUDIO_SINK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ALARM_AUDIO_SINK))
#define GST_ALARM_AUDIO_SINK_CAST(obj)       ((GstAlarmAudioSink *) (obj))

typedef struct _GstAlarmAudioSink GstAlarmAudioSink;
typedef struct _GstAlarmAudioSinkClass GstAlarmAudioSinkClass;

/**
 * GstAlarmAudioSink:
 *
 */
struct _GstAlarmAudioSink {
  GstBaseSink parent;

  GMutex lock;
  GstAlarmIPC *ipc;
};

struct _GstAlarmAudioSinkClass {
  GstBaseSinkClass parent_class;
};

GType gst_alarm_audio_sink_get_type(void);

G_END_DECLS

#endif /* __GST_ALARM_AUDIO_SINK_H__ */
