/*
 * Copyright © 2019 VXG Inc., Alarm.com. All rights reserved.
 *
 * Author: Ilya Smelykh <ilya@videoexpertsgroup.com>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include "gstalarmsrc.h"

GST_DEBUG_CATEGORY_STATIC (alarm_src_debug);
#define GST_CAT_DEFAULT alarm_src_debug

#define DEFAULT_BUFFER_SIZE 5
#define DEFAULT_CONNECTION GST_ALARM_IPC_CONNECTION_VIDEO_S1
#define DEFAULT_CODEC GST_ALARM_IPC_CODEC_NONE

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_CONNECTION,
  PROP_CODEC,
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

#define parent_class gst_alarm_src_parent_class
G_DEFINE_TYPE (GstAlarmSrc, gst_alarm_src, GST_TYPE_PUSH_SRC);

static void gst_alarm_src_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_alarm_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
static GstStateChangeReturn gst_alarm_src_change_state (GstElement * element,
    GstStateChange transition);
void gst_alarm_src_finalize (GObject * object);

/* GstBaseSrc vmethods */
static gboolean gst_alarm_src_start (GstAlarmSrc *src);
static gboolean gst_alarm_src_stop (GstAlarmSrc *src);
static gboolean gst_alarm_src_unlock (GstBaseSrc *src);
static gboolean gst_alarm_src_unlock_stop (GstBaseSrc *src);

/* GstPushSrc vmethods */
static GstFlowReturn
gst_alarm_src_create (GstPushSrc *src, GstBuffer **buf);

/* GObject vmethod implementations */

/* initialize the alarmsrc's class */
static void
gst_alarm_src_class_init (GstAlarmSrcClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseSrcClass *gstbasesrc_class;
  GstPushSrcClass *gstpushsrc_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  gstbasesrc_class = (GstBaseSrcClass *) klass;
  gstpushsrc_class = (GstPushSrcClass *) klass;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_alarm_src_change_state);

  gobject_class->set_property = gst_alarm_src_set_property;
  gobject_class->get_property = gst_alarm_src_get_property;
  gobject_class->finalize = gst_alarm_src_finalize;

  gstbasesrc_class->unlock = GST_DEBUG_FUNCPTR (gst_alarm_src_unlock);
  gstbasesrc_class->unlock_stop =
      GST_DEBUG_FUNCPTR (gst_alarm_src_unlock_stop);

  gstpushsrc_class->create = gst_alarm_src_create;

  g_object_class_install_property (gobject_class, PROP_CONNECTION,
      g_param_spec_enum ("connection", "IPC connection",
          "IPC connection",
          GST_TYPE_ALARM_IPC_CONNECTION, DEFAULT_CONNECTION,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS |
              G_PARAM_CONSTRUCT)));

  g_object_class_install_property (gobject_class, PROP_CODEC,
      g_param_spec_enum ("codec", "Stream Codec Type",
          "Stream Codec",
          GST_TYPE_ALARM_IPC_CODEC, DEFAULT_CODEC,
          (GParamFlags) (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS)));

  gst_element_class_set_static_metadata (gstelement_class, "Alarm IPC Video/Audio Source",
      "Source/Hardware", "Alarm IPC Video/Audio Source by VXG Inc.",
      "Ilya Smelykh <ilya@videoexpertsgroup.com>");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));

  GST_DEBUG_CATEGORY_INIT (alarm_src_debug, "alarmsrc", 0,
      "Alarm IPC source element");
}

static void
gst_alarm_src_init (GstAlarmSrc * self)
{
  self->silent = TRUE;

  gst_base_src_set_live (GST_BASE_SRC (self), TRUE);
  gst_base_src_set_do_timestamp (GST_BASE_SRC (self), TRUE);
  gst_base_src_set_format (GST_BASE_SRC (self), GST_FORMAT_TIME);
  gst_base_src_set_dynamic_size (GST_BASE_SRC (self), TRUE);

  g_mutex_init (&self->lock);
  g_cond_init (&self->cond);

  self->current_frames =
      gst_queue_array_new_for_struct (sizeof (fp_frm_t), DEFAULT_BUFFER_SIZE);

  self->silent = FALSE;

  /* Set correct codec type and base time on first frame from IPC */
  self->last_fp_seqnum = GST_SEQNUM_INVALID;
  self->internal_base_time = GST_CLOCK_TIME_NONE;
}

void
gst_alarm_src_finalize (GObject * object)
{
  GstAlarmSrc *self = GST_ALARM_SRC_CAST (object);

  g_mutex_lock (&self->lock);
  if (self->current_frames) {
    while (gst_queue_array_get_length (self->current_frames) > 0) {
      gst_queue_array_pop_head_struct (self->current_frames);
    }
    gst_queue_array_free (self->current_frames);
    self->current_frames = NULL;
  }
  g_mutex_unlock (&self->lock);

  g_mutex_clear (&self->lock);
  g_cond_clear (&self->cond);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_alarm_src_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAlarmSrc *self = GST_ALARM_SRC (object);

  switch (prop_id) {
    case PROP_CONNECTION:
      self->connection = (GstAlarmIPCConnectionEnum) g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_alarm_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstAlarmSrc *self = GST_ALARM_SRC (object);

  switch (prop_id) {
    case PROP_CONNECTION:
      g_value_set_enum (value, self->connection);
      break;
    case PROP_CODEC:
      g_value_set_enum (value, self->codec);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* GstBaseSrc vmethods */
static gpointer
gst_alarm_ipc_recv_routine (gpointer user_data)
{
  GstAlarmSrc *self = GST_ALARM_SRC_CAST (user_data);
  fp_frm_t *fp;

  while (self->running) {
    if (gst_alarm_ipc_poll (self->ipc)) {
      g_mutex_lock (&self->lock);
      fp = gst_alarm_ipc_get_fp (self->ipc);
      if (fp) {
        GST_DEBUG_OBJECT (self, "Got new frame, enqueue and notify, queue len %i",
                          gst_queue_array_get_length (self->current_frames));
        gst_queue_array_push_tail_struct (self->current_frames, fp);
        g_free (fp);
        g_cond_signal (&self->cond);
      }
      g_mutex_unlock (&self->lock);
    }
  }

  return NULL;
}

static gboolean
gst_alarm_src_start (GstAlarmSrc *src)
{
  GstAlarmSrc* self = GST_ALARM_SRC_CAST (src);
  self->ipc = gst_alarm_ipc_connect (self->connection);

  if (!self->ipc) {
    GST_ERROR_OBJECT (self, "Unable to connect to IPC bus");
    goto error;
  }

  self->running = TRUE;
  self->capture_thread =
      g_thread_new ("VideoCaptureThread", gst_alarm_ipc_recv_routine, self);

  GST_INFO_OBJECT (self, "Video capturing thread started");
  return TRUE;
error:
  GST_ERROR_OBJECT (self, "Unable to start capturing thread");
  return FALSE;
}

static gboolean
gst_alarm_src_stop (GstAlarmSrc *src)
{
  GstAlarmSrc* self = GST_ALARM_SRC_CAST (src);

  GST_INFO_OBJECT (self, "Stopping capturing thread");
  g_mutex_lock (&self->lock);
  self->running = FALSE;
  g_mutex_unlock (&self->lock);

  g_thread_join (self->capture_thread);
  GST_INFO_OBJECT (self, "Capturing thread stopped");

  return TRUE;
}

static gboolean
gst_alarm_src_unlock (GstBaseSrc * bsrc)
{
  GstAlarmSrc *self = GST_ALARM_SRC_CAST (bsrc);

  g_mutex_lock (&self->lock);
  GST_INFO_OBJECT (self, "Unlock");
  self->flushing = TRUE;
  g_cond_signal (&self->cond);
  g_mutex_unlock (&self->lock);

  return TRUE;
}

static gboolean
gst_alarm_src_unlock_stop (GstBaseSrc * bsrc)
{
  GstAlarmSrc *self = GST_ALARM_SRC_CAST (bsrc);

  g_mutex_lock (&self->lock);
  self->flushing = FALSE;
  GST_INFO_OBJECT (self, "Unlock stop");
  while (gst_queue_array_get_length (self->current_frames) > 0) {
    gst_queue_array_pop_head_struct (self->current_frames);
  }
  g_mutex_unlock (&self->lock);

  return TRUE;
}

/* GstPushSrc vmethods */
static GstFlowReturn
gst_alarm_src_create (GstPushSrc *src, GstBuffer **buffer)
{
  GstAlarmSrc* self = GST_ALARM_SRC_CAST (src);
  GstMapInfo info;
  fp_frm_t *fp;
  GstCaps *caps;
  gboolean caps_changed;
  GstClockTime timestamp;

  GST_DEBUG_OBJECT (src, "Create");
  g_mutex_lock (&self->lock);
  while (gst_queue_array_is_empty (self->current_frames) && !self->flushing) {
    GST_INFO_OBJECT (self, "Waiting for new frame");
    g_cond_wait (&self->cond, &self->lock);
  }

  if (self->flushing) {
    GST_DEBUG_OBJECT (self, "Woken up but flushing");
    g_mutex_unlock (&self->lock);
    return GST_FLOW_FLUSHING;
  }

  fp = gst_queue_array_pop_head_struct (self->current_frames);
  *buffer = gst_buffer_new_and_alloc ((size_t) fp->size);

  g_assert (gst_buffer_map (*buffer, &info, GST_MAP_WRITE));
  memcpy (info.data, 
      gst_alarm_ipc_get_frame_addr (self->ipc, fp->offset), fp->size);
  gst_buffer_unmap (*buffer, &info);

  timestamp = gst_util_uint64_scale (fp->pts, 1000, 1);
  if (!GST_CLOCK_TIME_IS_VALID (self->internal_base_time)) {
    GST_DEBUG_OBJECT (self, "Internal base time set to %"GST_TIME_FORMAT,
        GST_TIME_ARGS (timestamp));
    self->internal_base_time = timestamp;
  }

  /**
   * Live sources should generate timestamps as absolute_time - base_time,
   * IPC provides us timestamps so we use first frame timestamp as base_time
   * and timestamp of each frame as absolute_time
   */
  GST_BUFFER_TIMESTAMP (*buffer) = timestamp - self->internal_base_time;

  /* SPS/PPS/VPS and IDR may have same seqnum */
  if (gst_util_seqnum_compare (fp->seq, self->last_fp_seqnum) < 0 ||
      gst_util_seqnum_compare (fp->seq, self->last_fp_seqnum) > 1)
    gst_buffer_set_flags (*buffer, GST_BUFFER_FLAG_DISCONT);
  self->last_fp_seqnum = fp->seq;

  /* Treat audio, jpeg and IDR as key frames */
  gst_buffer_set_flags (*buffer, GST_BUFFER_FLAG_DELTA_UNIT);
  switch (fp->type) {
    case GST_ALARM_IPC_FP_SPS:
    /* fall through */
    case GST_ALARM_IPC_FP_PPS:
    /* fall through */
    case GST_ALARM_IPC_FP_VPS:
      gst_buffer_set_flags (*buffer, GST_BUFFER_FLAG_HEADER);
    /* fall through */
    case GST_ALARM_IPC_FP_IDR:
    /* fall through */
    case GST_ALARM_IPC_FP_AUDIO:
    /* fall through */
    case GST_ALARM_IPC_FP_JPEG:
      gst_buffer_unset_flags (*buffer, GST_BUFFER_FLAG_DELTA_UNIT);
    break;
    default:
    break;
  }

  if ((GstAlarmIPCCodecEnum) fp->codec != self->codec) {
    GEnumClass *klass = g_type_class_ref(GST_TYPE_ALARM_IPC_CODEC);

    GST_DEBUG ("Codec type changed from %s to %s, updating caps...",
               g_enum_get_value (klass, self->codec)->value_name,
               g_enum_get_value (klass, fp->codec)->value_name);
    g_type_class_unref(klass);

    self->codec = fp->codec;
    caps_changed = TRUE;
  }

  g_mutex_unlock (&self->lock);

  GST_DEBUG_OBJECT (self,
      "Outputting buffer %p with timestamp %" GST_TIME_FORMAT " and duration %"
      GST_TIME_FORMAT" size %d", *buffer, GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (*buffer)),
      GST_TIME_ARGS (GST_BUFFER_DURATION (*buffer)), gst_buffer_get_size(*buffer));

  if (caps_changed) {
    caps = gst_alarm_ipc_codec_get_caps (self->codec);
    gst_base_src_set_caps (GST_BASE_SRC_CAST (src), caps);
    gst_element_post_message (GST_ELEMENT_CAST (self),
        gst_message_new_latency (GST_OBJECT_CAST (self)));
    gst_caps_unref (caps);
  }
  return GST_FLOW_OK;
}

/* GstElement vmethod implementations */
static GstStateChangeReturn
gst_alarm_src_change_state (GstElement * element,
    GstStateChange transition)
{
  GstAlarmSrc *self = GST_ALARM_SRC_CAST (element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      self->flushing = FALSE;
      ret = GST_STATE_CHANGE_NO_PREROLL;
      break;
    default:
      break;
  }

  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;
  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:{
      GST_DEBUG_OBJECT (self, "Stopping stream");
      gst_alarm_src_stop (self);
      ret = GST_STATE_CHANGE_NO_PREROLL;
      break;
    }
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:{
      GST_DEBUG_OBJECT (self, "Starting stream");
      gst_alarm_src_start (self);
      break;
    }
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  return ret;
}
