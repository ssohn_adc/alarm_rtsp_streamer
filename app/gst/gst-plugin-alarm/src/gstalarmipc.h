/*
 * Copyright © 2019 VXG Inc., Alarm.com. All rights reserved.
 *
 * Author: Ilya Smelykh <ilya@videoexpertsgroup.com>
 */

#ifndef __GST_ALARM_IPC_H
#define __GST_ALARM_IPC_H

#include <gst/gst.h>
#include <gst/video/video.h>

#include <base/base.h>
#include <base/ipcmsg.h>

G_BEGIN_DECLS

typedef enum {
  GST_ALARM_IPC_CONNECTION_VIDEO_S1, /* /dev/ipc/stream/1 */
  GST_ALARM_IPC_CONNECTION_VIDEO_S2, /* /dev/ipc/stream/2 */
  GST_ALARM_IPC_CONNECTION_VIDEO_S3, /* /dev/ipc/stream/3 */

  GST_ALARM_IPC_CONNECTION_AUDIO_IN, /* /dev/ipc/ai/1 */
  GST_ALARM_IPC_CONNECTION_AUDIO_OUT,/* /dev/ipc/ao/1 */

  GST_ALARM_IPC_CONNECTION_EVENT,    /* /dev/ipc/ev.s */
} GstAlarmIPCConnectionEnum;
#define GST_TYPE_ALARM_IPC_CONNECTION (gst_alarm_ipc_connection_get_type ())
GType gst_alarm_ipc_connection_get_type (void);

typedef enum {
  GST_ALARM_IPC_FP_IDR = SU_FRM_TYPE_ISLICE,
  GST_ALARM_IPC_FP_P = SU_FRM_TYPE_PSLICE,
  GST_ALARM_IPC_FP_VI = SU_FRM_TYPE_VIRTUAL_ISLICE,
  GST_ALARM_IPC_FP_SEI = SU_FRM_TYPE_SEI,
  GST_ALARM_IPC_FP_SPS = SU_FRM_TYPE_SPS,
  GST_ALARM_IPC_FP_PPS = SU_FRM_TYPE_PPS,
  GST_ALARM_IPC_FP_VPS = SU_FRM_TYPE_VPS,
  GST_ALARM_IPC_FP_IP = SU_FRM_TYPE_IPSLICE,
  GST_ALARM_IPC_FP_JPEG = SU_FRM_TYPE_JPEG,
  GST_ALARM_IPC_FP_AUDIO = SU_FRM_TYPE_AUDIO,
} GstAlarmIPCFPTypeEnum;
#define GST_TYPE_ALARM_IPC_FP_TYPE (gst_alarm_ipc_fp_type_get_type ())
GType gst_alarm_ipc_fp_type_get_type (void);


typedef enum {
  GST_ALARM_IPC_CODEC_NONE = SU_CODEC_NONE,
  GST_ALARM_IPC_CODEC_MJPEG = SU_CODEC_MJPEG,
  GST_ALARM_IPC_CODEC_H264 = SU_CODEC_H264,
  GST_ALARM_IPC_CODEC_H265 = SU_CODEC_H265,

  GST_ALARM_IPC_CODEC_G711A = SU_CODEC_G711A,
  GST_ALARM_IPC_CODEC_G711U = SU_CODEC_G711U,
  GST_ALARM_IPC_CODEC_G726_16K = SU_CODEC_G726_16K,
  GST_ALARM_IPC_CODEC_G726_24K = SU_CODEC_G726_24K,
  GST_ALARM_IPC_CODEC_G726_32K = SU_CODEC_G726_32K,
  GST_ALARM_IPC_CODEC_G726_40K = SU_CODEC_G726_40K,
  GST_ALARM_IPC_CODEC_AAC = SU_CODEC_AAC,
  GST_ALARM_IPC_CODEC_PCM48K_16_CH1 = SU_CODEC_PCM48K_16_CH1,
  GST_ALARM_IPC_CODEC_PCM08K_16_CH1 = SU_CODEC_PCM08K_16_CH1,

  GST_ALARM_IPC_CODEC_IVA = SU_CODEC_EVENT_IVA,
} GstAlarmIPCCodecEnum;
#define GST_TYPE_ALARM_IPC_CODEC (gst_alarm_ipc_codec_get_type ())
GType gst_alarm_ipc_codec_get_type (void);
GstCaps* gst_alarm_ipc_codec_get_caps (GstAlarmIPCCodecEnum c);

typedef struct _GstAlarmIPC GstAlarmIPC;
typedef struct _GstAlarmIPCPrivate GstAlarmIPCPrivate;

struct _GstAlarmIPC {
  GstMiniObject parent;
  GstAlarmIPCConnectionEnum connection;

  int fd;
  fd_set fd_set;
  GstAlarmIPCPrivate *priv;

  /* Everything below protected by mutex */
  GMutex lock;
  GCond cond;
};

G_END_DECLS

GstAlarmIPC* gst_alarm_ipc_connect (GstAlarmIPCConnectionEnum connection);
void gst_alarm_ipc_disconnect (GstAlarmIPC* ipc);
gboolean gst_alarm_ipc_poll (GstAlarmIPC* ipc);
fp_frm_t* gst_alarm_ipc_get_fp (GstAlarmIPC* ipc);
int gst_alarm_ipc_write (GstAlarmIPC* ipc, guint8* data, gsize len);
uint8_t* gst_alarm_ipc_get_frame_addr (GstAlarmIPC* ipc, size_t offset);

#endif /* __GST_ALARM_IPC_H */

