/*
 * Copyright © 2019 VXG Inc., Alarm.com. All rights reserved.
 *
 * Author: Ilya Smelykh <ilya@videoexpertsgroup.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gstalarmsrc.h"
#include "gstalarmaudiosink.h"

static gboolean
plugin_init (GstPlugin * plugin)
{
  if (!gst_element_register (plugin, "alarmsrc", GST_RANK_NONE,
      GST_TYPE_ALARM_SRC))
    return FALSE;

  if (!gst_element_register (plugin, "alarmaudiosink", GST_RANK_NONE,
      GST_TYPE_ALARM_AUDIO_SINK))
    return FALSE;

  return TRUE;
}

#ifndef PACKAGE
#define PACKAGE "alarm"
#endif

GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    alarm,
    "alarm by VXG Inc.",
    plugin_init,
    VERSION,
    "LGPL",
    "GStreamer",
    "http://vxg.io/"
)
