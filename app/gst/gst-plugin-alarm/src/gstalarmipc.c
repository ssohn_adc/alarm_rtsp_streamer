/*
 * Copyright © 2019 VXG Inc., Alarm.com. All rights reserved.
 * Author: 2019 Ilya Smelykh <ilya@videoexpertsgroup.com>
 */

#include <sys/select.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#if USED_APR_SHM
#include <apr2/apr_general.h>
#include <apr2/apr_thread_proc.h>
#include <apr2/apr_shm.h>
#endif

#include "gstalarmipc.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

GST_DEBUG_CATEGORY_STATIC (gst_alarm_ipc_debug);
#define GST_CAT_DEFAULT gst_alarm_ipc_debug

struct _GstAlarmIPCPrivate
{
#if USED_APR_SHM
  apr_pool_t *apr_pool;
  apr_shm_t *shm;
#endif
  
  gchar* shared_mem;
};

static GstAlarmIPCPrivate *g_priv = NULL;

GType
gst_alarm_ipc_codec_get_type (void)
{
  static gsize id = 0;
  static const GEnumValue codec[] = {
    {GST_ALARM_IPC_CODEC_NONE, "None", "none"},
    {GST_ALARM_IPC_CODEC_MJPEG, "MJPEG", "mjpeg"},
    {GST_ALARM_IPC_CODEC_H264, "H264", "AVC"},
    {GST_ALARM_IPC_CODEC_H265, "H265", "HEVC"},

    {GST_ALARM_IPC_CODEC_G711A, "G711A", "A-LAW"},
    {GST_ALARM_IPC_CODEC_G711U, "G711U", "U-LAW"},
    {GST_ALARM_IPC_CODEC_G726_16K, "G726_16K", "G726 16kbps"},
    {GST_ALARM_IPC_CODEC_G726_24K, "G726_24K", "G726 24kbps"},
    {GST_ALARM_IPC_CODEC_G726_32K, "G726_32K", "G726 32kbps"},
    {GST_ALARM_IPC_CODEC_G726_40K, "G726_40K", "G726 40kbps"},
    {GST_ALARM_IPC_CODEC_AAC, "AAC", "aac mpeg audio"},
    {GST_ALARM_IPC_CODEC_PCM48K_16_CH1, "PCM48K_16_CH1", "PCM 48kHz 16bit mono"},
    {GST_ALARM_IPC_CODEC_PCM08K_16_CH1, "PCM08K_16_CH1", "PCM 8kHz 16bit mono"},

    {GST_ALARM_IPC_CODEC_IVA, "IVA", "iva?"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&id)) {
    GType tmp = g_enum_register_static ("GstAlarmIPCCodec",
                                        codec);
    g_once_init_leave (&id, tmp);
  }

  return (GType) id;
}

GstCaps*
gst_alarm_ipc_codec_get_caps (GstAlarmIPCCodecEnum c)
{
  GstCaps* caps = NULL;

  switch (c) {
  case GST_ALARM_IPC_CODEC_H264:
    caps = gst_caps_from_string("video/x-h264,stream-format=byte-stream");
  break;
  case GST_ALARM_IPC_CODEC_H265:
    caps = gst_caps_from_string("video/x-h265,stream-format=byte-stream");
  break;
  case GST_ALARM_IPC_CODEC_MJPEG:
    caps = gst_caps_from_string("image/jpeg");
  break;
  case GST_ALARM_IPC_CODEC_G711A:
    caps = gst_caps_from_string("audio/x-alaw,channels=1,rate=8000");
  break;
  case GST_ALARM_IPC_CODEC_G711U:
    caps = gst_caps_from_string("audio/x-mulaw,channels=1,rate=8000");
  break;
  case GST_ALARM_IPC_CODEC_G726_16K:
  case GST_ALARM_IPC_CODEC_G726_24K:
  case GST_ALARM_IPC_CODEC_G726_32K:
  case GST_ALARM_IPC_CODEC_G726_40K:
    caps = gst_caps_from_string("audio/x-adpcm,layout=g726");
  break;
  case GST_ALARM_IPC_CODEC_AAC:
    /* FIXME: figure out the exact format and version */
    caps = gst_caps_from_string("audio/mpeg,mpegversion=4,stream-format=raw");
  break;
  case GST_ALARM_IPC_CODEC_PCM08K_16_CH1:
    caps = gst_caps_from_string("audio/x-raw,channels=1,rate=8000,format=S16LE,layout=interleaved");
  break;
  case GST_ALARM_IPC_CODEC_PCM48K_16_CH1:
    caps = gst_caps_from_string("audio/x-raw,channels=1,rate=48000,format=S16LE,layout=interleaved");
  break;
  default:
    GST_ERROR ("Unable to get create caps for IPC codec type %d", c);
    caps = NULL;
  }

  return caps;
}

GType
gst_alarm_ipc_fp_type_get_type (void)
{
  static gsize id = 0;
  static const GEnumValue frame_pointer_type[] = {
    {GST_ALARM_IPC_FP_P, "P", "p slice"},
    {GST_ALARM_IPC_FP_IDR, "IDR", "idr slice"},
    {GST_ALARM_IPC_FP_VI, "VI", "virtual slice"},
    {GST_ALARM_IPC_FP_SEI, "SEI", "sei message"},
    {GST_ALARM_IPC_FP_SPS, "SPS", "sps"},
    {GST_ALARM_IPC_FP_PPS, "PPS", "pps"},
    {GST_ALARM_IPC_FP_VPS, "VPS", "hevc vps"},
    {GST_ALARM_IPC_FP_IP, "IP", "ip slice"},
    {GST_ALARM_IPC_FP_JPEG, "JPEG", "jpeg"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&id)) {
    GType tmp = g_enum_register_static ("GstAlarmIPCFPType",
                                        frame_pointer_type);
    g_once_init_leave (&id, tmp);
  }

  return (GType) id;
}


GType
gst_alarm_ipc_connection_get_type (void)
{
  static gsize id = 0;
  static const GEnumValue connections[] = {
    {GST_ALARM_IPC_CONNECTION_VIDEO_S1, "Video S1", "/dev/ipc/stream/1"},
    {GST_ALARM_IPC_CONNECTION_VIDEO_S2, "Video S2", "/dev/ipc/stream/2"},
    {GST_ALARM_IPC_CONNECTION_VIDEO_S3, "Video S3", "/dev/ipc/stream/3"},
    {GST_ALARM_IPC_CONNECTION_AUDIO_IN, "Audio input", "/dev/ipc/ai/1"},
    {GST_ALARM_IPC_CONNECTION_AUDIO_OUT, "Audio output", "/dev/ipc/ao/1"},
    {GST_ALARM_IPC_CONNECTION_EVENT, "Event", "/dev/ipc/ev.s"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&id)) {
    GType tmp = g_enum_register_static ("GstAlarmIPCConnection", connections);
    g_once_init_leave (&id, tmp);

    GST_DEBUG_CATEGORY_INIT (gst_alarm_ipc_debug, "alarmipc",
        0, "alarmipc");
  }

  return (GType) id;
}

static const gchar*
gst_alarm_ipc_connection_get_path (GstAlarmIPCConnectionEnum connection)
{
  switch (connection)
  {
  case GST_ALARM_IPC_CONNECTION_VIDEO_S1:
  case GST_ALARM_IPC_CONNECTION_VIDEO_S2:
  case GST_ALARM_IPC_CONNECTION_VIDEO_S3:
  case GST_ALARM_IPC_CONNECTION_AUDIO_IN:
  case GST_ALARM_IPC_CONNECTION_AUDIO_OUT:
  case GST_ALARM_IPC_CONNECTION_EVENT: {
    GEnumClass *klass = g_type_class_ref(GST_TYPE_ALARM_IPC_CONNECTION);
    const gchar *ret = g_enum_get_value (klass, connection)->value_nick;
    g_type_class_unref(klass);
    return ret;
  }

  default:
      GST_ERROR ("Unknown connection id %d", connection);
      break;
  }

  return NULL;
}

static int set_non_blocking_mode(int sock)
{
    int flags;

    flags = fcntl(sock, F_GETFL, 0);
    (void) fcntl(sock, F_SETFL, flags | O_NONBLOCK);

    return 0;
}

#define SHARED_MEM_DEV "/dev/shm/fp"
GstAlarmIPC* 
gst_alarm_ipc_connect (GstAlarmIPCConnectionEnum connection)
{
  GstAlarmIPC *alarm_ipc;
  struct sockaddr_un vun;
  size_t vsize = sizeof (vun);

  alarm_ipc = g_new0(GstAlarmIPC, 1);
  alarm_ipc->connection = connection;
  g_mutex_init(&alarm_ipc->lock);
  g_cond_init(&alarm_ipc->cond);

  /* Code based on Ambarella S2E sample,
   * no idea what is going on for audio out
   */
  if (connection == GST_ALARM_IPC_CONNECTION_AUDIO_OUT) {
    ipcmsg_resp_code_t rc;
    struct sockaddr_un un;
    int len, r;
    char req_audio_str[1024] = { 0 };
    ipcmsg_hdr_t hdr;

    alarm_ipc->fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if (alarm_ipc->fd < 0) {
      GST_ERROR ("audio talk failed due to no available fd\n");
      GST_ERROR("errno: %s\n", strerror(errno));
      goto unix_socket_fail;
    }
    //////////////////////////////////////////////////////////////////
    //set_params .... MU-LAW!!!, channels: 1, rate: 8000//
    //////////////////////////////////////////////////////////////////
    un.sun_family = AF_UNIX;
    sprintf(un.sun_path, "%s/c%d",
            g_path_get_dirname (gst_alarm_ipc_connection_get_path (connection)),
            alarm_ipc->fd);
    unlink(un.sun_path);
    len = offsetof(struct sockaddr_un, sun_path) + strlen(un.sun_path);
    bind(alarm_ipc->fd, (struct sockaddr *) &un, len);

    //connect ipc//
    memset(&un, 0, sizeof(un));
    un.sun_family = AF_UNIX;
    sprintf(un.sun_path, gst_alarm_ipc_connection_get_path (connection));
    if (connect(alarm_ipc->fd, (struct sockaddr *) &un, sizeof un) < 0) {
      GST_ERROR ("connect %s error\n", un.sun_path);
      GST_ERROR("errno: %s\n", strerror(errno));
      goto unix_socket_fail;
    }

    snprintf(req_audio_str, sizeof(req_audio_str), "%s://%s%s", "http",
         "127.0.0.1", "audioTalk");

#ifdef MODEL_PLATFORM_V622
    hdr.type = IPCMSG_TYPE_REQUEST_AUDIO_OUT;
    hdr.length = strlen(req_audio_str) + 1;

    write(alarm_ipc->fd, &hdr, sizeof(hdr));
    write(alarm_ipc->fd, req_audio_str, hdr.length);
    r = recv(alarm_ipc->fd, &hdr, sizeof(hdr), MSG_NOSIGNAL);
    if (r != sizeof(hdr) || hdr.type != IPCMSG_TYPE_RESPONSE_CODE) {
      GST_ERROR ("recv ipcmsg failed, r=%d, hdr.type=%04x\n", r, hdr.type);
      goto unix_socket_fail;
    }
    r = recv(alarm_ipc->fd, &rc, sizeof(rc), MSG_NOSIGNAL);
    if (r != sizeof(rc) || rc.code != IPCMSG_RESP_CODE_OK) {
      printf("recv ipcmsg failed, r=%d, rc.code=%04x\n", r, rc.code);
      goto unix_socket_fail;
    }
#else
    hdr.type = IPCMSG_TYPE_REQUEST_AUDIO_OUT_ADC;
    hdr.length = 0;
    write(alarm_ipc->fd, &hdr, sizeof(ipcmsg_hdr_t));
#endif

    if (set_non_blocking_mode (alarm_ipc->fd) != 0) {
      GST_WARNING ("set_non_blocking_mode error\n");
    }
  }
  else {
    if (!g_priv) {
      int fd = -1;
      g_priv = g_new0 (GstAlarmIPCPrivate, 1);
#if USED_APR_SHM
      apr_initialize ();
      apr_pool_create (&g_priv->apr_pool, NULL);
      g_priv->shm = NULL;
      apr_shm_attach (&g_priv->shm, SHARED_MEM_DEV, g_priv->apr_pool);
      g_priv->shared_mem = (char *) apr_shm_baseaddr_get (g_priv->shm);
#else
      fd = shm_open ("fp", O_RDWR, 0644);
      if (fd > 0) {
          size_t size = lseek(fd, 0, SEEK_END);
          g_priv->shared_mem = (char*)mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
          close(fd);
      } else {
          GST_ERROR ("failed to open shared memory error: %s\n", strerror(errno));
          goto unix_socket_fail;
      }
#endif
    }

    alarm_ipc->priv = g_priv;

    /* Unix socket for IPC */
    alarm_ipc->fd = socket (PF_UNIX, SOCK_STREAM, 0);
    if (alarm_ipc->fd > 0) {
      memset (&vun, 0, sizeof (vun));
      vun.sun_family = AF_UNIX;
      strcpy (vun.sun_path, gst_alarm_ipc_connection_get_path(connection));

      if (connect (alarm_ipc->fd, (struct sockaddr *) &vun, vsize) < 0) {
         GST_ERROR ("failed to connect to unix socket for ipc: %s\n", strerror(errno));
        goto unix_socket_fail;
      }
      else {
        GST_INFO ("IPC connect %s success", vun.sun_path);
      }
    }
  }

  return alarm_ipc;
unix_socket_fail:
  GST_ERROR ( "IPC connect fail. (%s)", vun.sun_path);

  close (alarm_ipc->fd);
  g_free(alarm_ipc);

  return NULL;
}

void
gst_alarm_ipc_disconnect(GstAlarmIPC* ipc)
{
  close (ipc->fd);
  g_free(ipc);
}

gboolean 
gst_alarm_ipc_poll(GstAlarmIPC* ipc)
{
  struct timeval tv = {
    .tv_sec = 3,
    .tv_usec = 0
  };
  int rc = 0;

  if (ipc->connection == GST_ALARM_IPC_CONNECTION_AUDIO_OUT) {
    GST_WARNING ("Polling Audio Output channel");
    return FALSE;
  }

  FD_ZERO (&ipc->fd_set);
  FD_SET (ipc->fd, &ipc->fd_set);
  GST_DEBUG ("Before select");
  rc = select (ipc->fd + 1, &ipc->fd_set, NULL, NULL, &tv);
  GST_DEBUG ("After select ret %d", rc);
  if (rc < 0) {
    if (errno != EINTR) {
      GST_ERROR ("Poll abort");
    }
  }
  else if (rc == 0) {
    GST_DEBUG ("IPC poll timedout");
    /* Timeout */
  }
  else {
    if (FD_ISSET (ipc->fd, &ipc->fd_set))
      return TRUE;
  }
  
  return FALSE;
}

#define SU_FRM_INVALID(base, frm) (memcmp((base) + (frm).offset, (frm).magic, 4))
fp_frm_t*
gst_alarm_ipc_get_fp (GstAlarmIPC* ipc)
{
  fp_frm_t *fp = g_new0(fp_frm_t, 1);
  int ret = read (ipc->fd, fp, sizeof (*fp));

  if (ret == sizeof (*fp) && g_priv && !SU_FRM_INVALID (g_priv->shared_mem, *fp)) {
    GST_DEBUG ("Got IPC FP: codec:%d type:%i magic:[0x%x 0x%x 0x%x 0x%x] "
        "seq:%d pts:%llu offset:%u size:%zu",
        fp->codec, fp->type,
        fp->magic[0], fp->magic[1], fp->magic[2], fp->magic[3],
        fp->seq, fp->pts, fp->offset, fp->size);
    return fp;
  }

  g_free (fp);
  GST_ERROR ("Unable to get frame pointer from IPC");
  return NULL;
}

uint8_t*
gst_alarm_ipc_get_frame_addr (GstAlarmIPC* ipc, size_t offset)
{
  return (uint8_t*) (ipc->priv->shared_mem + offset);
}

int
gst_alarm_ipc_write (GstAlarmIPC* ipc, guint8* data, gsize len)
{
  gsize total = 0;
  while (ipc && total != len) {
    int rlen = write (ipc->fd, data + total, len - total);
    if (rlen <= 0 && errno != EINTR && errno != EAGAIN)
        return -1;
    total += rlen;
  }

  GST_DEBUG ("%d bytes successfully sent to alarm ipc", total);

  return total;
}
