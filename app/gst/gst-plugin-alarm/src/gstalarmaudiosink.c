/*
 * Copyright © 2019 VXG Inc., Alarm.com. All rights reserved.
 *
 * Author: Ilya Smelykh <ilya@videoexpertsgroup.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gst/gst.h>
#include "gstalarmaudiosink.h"

GST_DEBUG_CATEGORY_STATIC (alarm_audio_sink_debug);
#define GST_CAT_DEFAULT alarm_audio_sink_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
};

/* the capabilities of the input
 *
 */
static GstStaticPadTemplate pad_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-mulaw,channels=1,rate=8000")
    );

#define parent_class gst_alarm_audio_sink_parent_class
G_DEFINE_TYPE (GstAlarmAudioSink, gst_alarm_audio_sink, GST_TYPE_BASE_SINK)

static void gst_alarm_audio_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_alarm_audio_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);
void gst_alarm_audio_sink_finalize (GObject * object);

/* GstBaseSink vmethods */
static gboolean gst_alarm_audio_sink_start (GstBaseSink *sink);
static gboolean gst_alarm_audio_sink_stop (GstBaseSink *sink);
static GstFlowReturn gst_alarm_audio_sink_render (GstBaseSink *sink,
    GstBuffer *buf);

/* initialize the alarmaudiosink class */
static void
gst_alarm_audio_sink_class_init (GstAlarmAudioSinkClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseSinkClass *gstbasesink_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  gstbasesink_class = (GstBaseSinkClass *) klass;

  gobject_class->set_property = gst_alarm_audio_sink_set_property;
  gobject_class->get_property = gst_alarm_audio_sink_get_property;
  gobject_class->finalize = gst_alarm_audio_sink_finalize;

  gstbasesink_class->start =
      GST_DEBUG_FUNCPTR (gst_alarm_audio_sink_start);
  gstbasesink_class->stop =
      GST_DEBUG_FUNCPTR (gst_alarm_audio_sink_stop);
  gstbasesink_class->render =
      GST_DEBUG_FUNCPTR (gst_alarm_audio_sink_render);

  gst_element_class_set_static_metadata (gstelement_class, "Alarm IPC Audio Sink",
      "Sink/Hardware", "Alarm IPC Audio Sink by VXG Inc.",
      "Ilya Smelykh <ilya@videoexpertsgroup.com>");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&pad_factory));

  GST_DEBUG_CATEGORY_INIT (alarm_audio_sink_debug, "alarmaudiosink", 0,
      "Alarm IPC sink element");
}

static void
gst_alarm_audio_sink_init (GstAlarmAudioSink * self)
{
  g_mutex_init (&self->lock);

  self->ipc = NULL;
}

void
gst_alarm_audio_sink_finalize (GObject * object)
{
  GstAlarmAudioSink *self = GST_ALARM_AUDIO_SINK_CAST (object);

  g_mutex_clear (&self->lock);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_alarm_audio_sink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstAlarmAudioSink *self G_GNUC_UNUSED = GST_ALARM_AUDIO_SINK (object);

  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_alarm_audio_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstAlarmAudioSink *self G_GNUC_UNUSED = GST_ALARM_AUDIO_SINK (object);

  switch (prop_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* GstBaseSink vmethods implementation */
static gboolean
gst_alarm_audio_sink_start (GstBaseSink *sink)
{
  GstAlarmAudioSink *self = GST_ALARM_AUDIO_SINK (sink);
  GstBaseSink *base = GST_BASE_SINK (sink);

  self->ipc = gst_alarm_ipc_connect (GST_ALARM_IPC_CONNECTION_AUDIO_OUT);
  if (!self->ipc)
    return FALSE;

  GST_INFO_OBJECT (self, "IPC connection created, async state change: %d",
                   gst_base_sink_is_async_enabled (base));

  return TRUE;
}

static gboolean
gst_alarm_audio_sink_stop (GstBaseSink *sink)
{
  GstAlarmAudioSink *self = GST_ALARM_AUDIO_SINK (sink);

  if (self->ipc)
    gst_alarm_ipc_disconnect (self->ipc);

  GST_INFO_OBJECT (self, "IPC connection destroyed");

  return TRUE;
}

static GstFlowReturn
gst_alarm_audio_sink_render (GstBaseSink *sink, GstBuffer *buf)
{
  GstAlarmAudioSink *self = GST_ALARM_AUDIO_SINK (sink);
  GstMapInfo info;
  int res = -1;
  GstFlowReturn ret = GST_FLOW_ERROR;

  if (gst_buffer_map (buf, &info, GST_MAP_READ)) {
      GST_INFO_OBJECT (self,
          "Sending buffer: %p size: %d timestamp: %"GST_TIME_FORMAT, buf,
          info.size, GST_TIME_ARGS (GST_BUFFER_PTS (buf)));

     res = gst_alarm_ipc_write (self->ipc, info.data, info.size);
     ret = (res == info.size ? GST_FLOW_OK : GST_FLOW_ERROR);
     gst_buffer_unmap (buf, &info);
  }

  return ret;
}
