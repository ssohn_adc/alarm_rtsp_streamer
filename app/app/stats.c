/*************************************************************************************
* @file stats.c
*
* app statistics
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include "stats.h"

#include <json-glib/json-glib.h>

#define str(s) #s

#define STAT_ITEM(name, type) {str(name), type, offsetof(Stats, name), sizeof(((Stats*)0)->name)}

typedef enum {
    STAT_TYPE_BOOL = 0,
    STAT_TYPE_STRING,
    STAT_TYPE_INT,
    STAT_TYPE_DOUBLE
} StatValueType;

typedef struct {
    gchar const* key;
    StatValueType type;
    size_t offset;
    size_t max_size;
} StatItem;

struct _Stats
{
    GSource* src;
    gchar* output_file;
    guint interval;
    GTimer* timer;

    /* statistics */
    guint uptime_secs;
    guint total_sessions;
    guint auth_failures;
    guint ss_connects;
    guint ss_disconnects;
};

static StatItem stat_items[] =
{
    STAT_ITEM(uptime_secs, STAT_TYPE_INT),
    STAT_ITEM(total_sessions, STAT_TYPE_INT),
    STAT_ITEM(auth_failures, STAT_TYPE_INT),
    STAT_ITEM(ss_connects, STAT_TYPE_INT),
    STAT_ITEM(ss_disconnects, STAT_TYPE_INT)
};

static StatsResult s_stats_create_json(Stats* stats, JsonObject** json_obj)
{
    JsonObject* main_obj = json_object_new();
    if (main_obj) {
        gint i;
        for (i = 0; i < sizeof(stat_items) / sizeof(stat_items[0]); ++i) {
            switch (stat_items[i].type) {
            case STAT_TYPE_STRING:
                json_object_set_string_member(main_obj, stat_items[i].key, (gchar*) (stats) + stat_items[i].offset);
                break;
            case STAT_TYPE_BOOL:
                json_object_set_boolean_member(main_obj, stat_items[i].key, *((gboolean*) ((gchar*) (stats) + stat_items[i].offset)));
                break;
            case STAT_TYPE_INT:
                json_object_set_int_member(main_obj, stat_items[i].key, *((gint*) ((gchar*) (stats) + stat_items[i].offset)));
                break;
            case STAT_TYPE_DOUBLE:
                json_object_set_double_member(main_obj, stat_items[i].key, *((gdouble*) ((gchar*) (stats) + stat_items[i].offset)));
                break;
            default:
                return STATS_RESULT_ERR_UNKNOWN_STAT_TYPE;
            }
        }
        *json_obj = main_obj;
    }

    return STATS_RESULT_OK;
}

static void s_stats_write_to_file(Stats* stats)
{
    JsonNode *root = NULL;
    JsonGenerator *generator = NULL;
    GError *err = NULL;
    StatsResult result;

    JsonObject *main_obj = NULL;

    stats->uptime_secs = g_timer_elapsed(stats->timer, NULL);

    result = s_stats_create_json(stats, &main_obj);
    if (result != STATS_RESULT_OK || !main_obj) {
        return;
    }

    root = json_node_init_object(json_node_alloc(), main_obj);
    generator = json_generator_new();
    json_generator_set_root(generator, root);

    json_generator_set_pretty(generator, TRUE);
    if (!json_generator_to_file(generator, stats->output_file, &err)) {
        g_object_unref(generator);
        json_node_free(root);
        json_object_unref(main_obj);
        return;
    }

    g_object_unref(generator);
    json_node_free(root);
    json_object_unref(main_obj);
}

static gboolean s_stats_write_callback(Stats* stats)
{
    s_stats_write_to_file(stats);
    return G_SOURCE_CONTINUE;
}


/*********************************************************************/
/******************************** API ********************************/
/*********************************************************************/
void stats_increment_total_sessions(Stats* stats)
{
    if (stats) {
        stats->total_sessions++;
        s_stats_write_to_file(stats);
    }
}

void stats_increment_auth_failures(Stats* stats)
{
    if (stats) {
        stats->auth_failures++;
        s_stats_write_to_file(stats);
    }
}

void stats_increment_ss_connects(Stats* stats)
{
    if (stats) {
        stats->ss_connects++;
        s_stats_write_to_file(stats);
    }
}

void stats_increment_ss_disconnects(Stats* stats)
{
    if (stats) {
        stats->ss_disconnects++;
        s_stats_write_to_file(stats);
    }
}

Stats* stats_new(GMainContext* ctx, gchar const* output_file, guint interval)
{
    Stats* self = g_malloc0(sizeof(Stats));
    if (self) {
        self->output_file = g_strdup(output_file);
        self->interval = interval;
        GSource* src = g_timeout_source_new_seconds(self->interval);
        if (!src) {
            stats_destroy(&self);
            return NULL;
        }
        g_source_set_callback(src, (GSourceFunc)s_stats_write_callback, self, NULL);
        g_source_attach(src, ctx);
        self->src = src;

        GTimer* timer = g_timer_new();
        if (!timer) {
            g_source_destroy(self->src);
            stats_destroy(&self);
        }
        self->timer = timer;
        g_timer_start(self->timer);
    }

    return self;
}

void stats_destroy(Stats** self_p)
{
    if (self_p) {
        Stats* self = *self_p;
        if (self) {
            g_timer_destroy(self->timer);
            g_source_destroy(self->src);
            if (self->output_file) {
                g_unlink(self->output_file);
                g_free(self->output_file);
            }
            g_free(self);
            *self_p = NULL;
        }
    }
}
