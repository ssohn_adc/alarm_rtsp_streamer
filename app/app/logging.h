/*************************************************************************************
* @file logging.h
*
* app log handlers
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/

#ifndef LOGGING_H
#define LOGGING_H

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef GST_USE_UNSTABLE_API
#define GST_USE_UNSTABLE_API
#endif
#include <gst/webrtc/webrtc_fwd.h>

void
syslog_gst_debug_log(GstDebugCategory *category, GstDebugLevel level,
        const gchar *file, const gchar *function, gint line, GObject *object,
        GstDebugMessage *message, gpointer user_data);

void
klog_gst_debug_log(GstDebugCategory *category, GstDebugLevel level,
        const gchar *file, const gchar *function, gint line, GObject *object,
        GstDebugMessage *message, gpointer user_data);

#ifdef __cplusplus
}
#endif

#endif /* LOGGING_H_ */
