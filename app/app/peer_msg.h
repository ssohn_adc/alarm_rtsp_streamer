/*************************************************************************************
* @file peer_msg.h
*
* This class handles the creation of peer messages to send over to the watcher.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#ifndef PEER_MSG_H
#define PEER_MSG_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <json-glib/json-glib.h>


/**
 * @brief peer message type
 */
typedef enum {
    PEER_MSG_TYPE_INFO = 0,     //!< informational message
    PEER_MSG_TYPE_ERROR         //!< error message
} PeerMsgType;

typedef struct _PeerMsg PeerMsg;

/**
 * @brief converts peer message to json string format
 * @param msg
 * @return pointer to json string
 */
extern gchar const* peer_msg_to_json_string(PeerMsg* msg);

/**
 * @brief create PeerMsg object
 * @return PeerMsg object if success, otherwise NULL for failure
 */
extern PeerMsg* peer_msg_new(PeerMsgType type, gchar const *src_peer_id,
        gchar const *dest_peer_id, gchar const *msg);

/**
 * @brief destroys a PeerMsg object and sets ptr to NULL
 * @param msg_p pointer to PeerMsg object
 */
extern void peer_msg_destroy(PeerMsg** msg_p);

#ifdef __cplusplus
}
#endif

#endif /* PEER_MSG_H */
