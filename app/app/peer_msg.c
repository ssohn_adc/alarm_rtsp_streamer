/*************************************************************************************
* @file peer_msg.c
*
* This class handles the creation of peer messages to send over to the watcher.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include <openssl/hmac.h>
#include <glib/gprintf.h>

#include "peer_msg.h"

struct _PeerMsg {
    PeerMsgType type;
    gchar* json_str;
};

/*********************************************************************/
/******************************** API ********************************/
/*********************************************************************/
gchar const* peer_msg_to_json_string(PeerMsg* msg)
{
    return msg->json_str;
}

PeerMsg* peer_msg_new(PeerMsgType type, gchar const *src_peer_id,
        gchar const *dest_peer_id, gchar const *msg)
{
    if (!src_peer_id || !dest_peer_id || !msg)
        return NULL;

    PeerMsg* self = g_malloc0(sizeof(PeerMsg));
    if (self) {
        self->type = type;
        self->json_str = NULL;
        JsonNode* root = NULL;
        JsonGenerator* generator = NULL;
        JsonObject* main_obj = json_object_new();

        if (!main_obj) {
            peer_msg_destroy(&self);
            return NULL;
        }

        json_object_set_string_member(main_obj, "to", dest_peer_id);
        json_object_set_string_member(main_obj,
                self->type == PEER_MSG_TYPE_ERROR ? "error" : "message", msg);
        json_object_set_string_member(main_obj, "from", src_peer_id);

        root = json_node_init_object(json_node_alloc(), main_obj);
        generator = json_generator_new();
        json_generator_set_root(generator, root);
        self->json_str = json_generator_to_data(generator, NULL);

        g_object_unref(generator);
        json_node_free(root);
        json_object_unref(main_obj);

    }

    return self;
}

void peer_msg_destroy(PeerMsg** self_p)
{
    if (self_p) {
        PeerMsg* self = *self_p;
        if (self) {
            if (self->json_str)
                g_free(self->json_str);
            g_free(self);
            *self_p = NULL;
        }
    }
}
