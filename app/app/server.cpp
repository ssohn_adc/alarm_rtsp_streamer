#include "server.h"

static void ClientConnected(GstRTSPServer* gstrtspserver, GstRTSPClient* arg1, gpointer user_data)
{
  GstRTSPConnection* connection = gst_rtsp_client_get_connection(arg1);
  if (!connection)
  {
    return;
  }
  //gst_rtsp_connection_set_tunneled(connection, TRUE);
  //gst_rtsp_connection_set_http_mode(connection, TRUE);
}

RtspServer* RtspServer::instance_ = nullptr;

bool RtspServer::Initialize() {
  gst_init(NULL, NULL);

  serverctx.loop = g_main_loop_new(NULL, FALSE);

  serverctx.server = gst_rtsp_server_new();
  serverctx.mounts = gst_rtsp_server_get_mount_points(serverctx.server);
  gst_rtsp_server_set_address(serverctx.server, serverctx.config->address.c_str());
  gst_rtsp_server_set_service(serverctx.server, serverctx.config->port.c_str());
  auto&& session = gst_rtsp_session_new("New session");
  gst_rtsp_session_prevent_expire(session);

  serverctx.factory = gst_rtsp_media_factory_new();

  gst_rtsp_media_factory_set_shared(serverctx.factory, TRUE);
  gst_rtsp_media_factory_set_latency(serverctx.factory, 0);
  gst_rtsp_mount_points_add_factory(serverctx.mounts, serverctx.config->route.c_str(), serverctx.factory);
  g_object_unref(serverctx.mounts);

  gulong clientConnectedConfigureId = g_signal_connect(serverctx.server, "client-connected", (GCallback)ClientConnected, this);

  /* Pipeline launch */
  std::string launchCmd = "";

  launchCmd += "videotestsrc ";
  //launchCmd += "pattern=snow";

  std::cout << "Resolution:\t" << 1280 << "x"
    << 720 << std::endl
    << std::endl;
  launchCmd += " ! videoscale ! video/x-raw,width=";
  launchCmd += "1280";
  launchCmd += ",height=";
  launchCmd += "720";
  launchCmd += " ! capsfilter ! queue ! openh264enc";
  launchCmd += " ! rtph264pay name=pay0 pt=96";

  g_print("Launching stream with the following pipeline: %s\n",
    launchCmd.c_str());
  gst_rtsp_media_factory_set_launch(serverctx.factory, launchCmd.c_str());


  gst_rtsp_media_factory_add_role(serverctx.factory, "user",
    GST_RTSP_PERM_MEDIA_FACTORY_ACCESS, G_TYPE_BOOLEAN, TRUE,
    GST_RTSP_PERM_MEDIA_FACTORY_CONSTRUCT, G_TYPE_BOOLEAN, TRUE, NULL);

  GstRTSPAuth* auth;
  GstRTSPToken* token;

  auth = gst_rtsp_auth_new();
  gst_rtsp_auth_set_supported_methods(auth, GST_RTSP_AUTH_DIGEST);

  token =
    gst_rtsp_token_new(GST_RTSP_TOKEN_MEDIA_FACTORY_ROLE, G_TYPE_STRING,
      "user", NULL);
  gst_rtsp_auth_add_digest(auth, "user", "password", token);
  gst_rtsp_token_unref(token);

  //gst_rtsp_server_set_auth(serverctx.server, auth);
  gst_rtsp_server_set_auth(serverctx.server, NULL);
  g_object_unref(auth);

#if 0
  GInetAddress* addr;
    GResolver* resolver;
      GError* err = NULL;
GSocketAddress *saddr;
gint bound_port = 0;
GSocket *http_socket;
int http_port = 4953;

        addr = g_inet_address_new_from_string("192.168.1.144");
	  if (!addr) {
		      GList* results;

		          resolver = g_resolver_get_default();

			      results =
				            g_resolver_lookup_by_name(resolver, "192.168.1.144", NULL, &err);
			          if (!results)
					        return false;

				      addr = G_INET_ADDRESS(g_object_ref(results->data));

				          g_resolver_free_addresses(results);
					      g_object_unref(resolver);
					        }
	    gchar* ip = g_inet_address_to_string(addr);
	      printf("The IP Address: %s\n", ip);

  saddr = g_inet_socket_address_new (addr, http_port);
    g_object_unref (addr);

      /* create the server listener socket */
      http_socket =
	            g_socket_new (g_socket_address_get_family (saddr), G_SOCKET_TYPE_STREAM,
				          G_SOCKET_PROTOCOL_TCP, &err);
        if (!http_socket)
		    return false;

	  printf("opened receiving server socket\n");

	    /* bind it */
	    printf("binding server socket to address\n");
	      if (!g_socket_bind (http_socket, saddr, TRUE, &err))
		          return false;

	        g_object_unref (saddr);


	    printf("HTTP socket is ready \n");

	    gboolean ret = gst_rtsp_server_transfer_connection(serverctx.server, http_socket,  "192.168.1.87", 80, NULL);
	    if(ret == FALSE) {
		    printf("HTTP transfer NOT succesfull \n");
	    } else {
		    printf("HTTP transfer succesfull \n");
	    }	 
#endif

  if (gst_rtsp_server_attach(serverctx.server, NULL) == 0) {
    return false;
  }

  g_main_loop_run(serverctx.loop);

  return true;
}

void RtspServer::SetDefaults() {
  serverctx.config = std::make_shared<ServerConfig>();
  serverctx.config->usrname = "";
  serverctx.config->pass = "";
  serverctx.config->route = "/live.sdp";
  serverctx.config->address = "0.0.0.0";
  serverctx.config->port = "8554";
  serverctx.config->input = "";
  serverctx.config->source_type = SourceType::RTSP_SOURCE;
}
