/*************************************************************************************
* @file app_config.c
*
* This class handles reading, parsing, and writing to the JSON configuration file
* that this process requires for operation.
*
**************************************************************************************
*
* Alarm.com Copyright 2000-2021
*
* COPYRIGHT STATEMENT.
* This software has been provided pursuant to a License Agreement containing
* restrictions on its use.  The software contains confidential information,
* trade secrets, patents (pending or approved) and proprietary information of
* Alarm.com and is protected by Federal copyright law.  This
* confidential information or source code files, should not be copied or distributed,
* in any form or medium, disclosed to any third party, or taken outside
* Alarm.com or authorized offices or used in any manner, except with prior
* written agreement with Alarm.com . All rights reserved.
*
* Alarm.com
*
*************************************************************************************/
#include "app_config.h"
#include <gst/gst.h>
#include <json-glib/json-glib.h>

#define str(s) #s

GST_DEBUG_CATEGORY_STATIC (app_config);
#define GST_CAT_DEFAULT app_config

#define GENERAL_CONFIG_PARAMETER(name, type) {str(name), type, offsetof(AppConfig, name), sizeof(((AppConfig*)0)->name)}
#define WEBRTC_CONFIG_PARAMETER(name, type) {str(name), type, offsetof(WebrtcConfig, name), sizeof(((WebrtcConfig*)0)->name)}
#define SOUP_CONFIG_PARAMETER(name, type) {str(name), type, offsetof(SoupConfig, name), sizeof(((SoupConfig*)0)->name)}

typedef enum {
    CONFIG_TYPE_BOOL = 0,
    CONFIG_TYPE_STRING,
    CONFIG_TYPE_INT,
    CONFIG_TYPE_DOUBLE
} ConfigValueType;

typedef struct {
    gchar const* key;
    ConfigValueType type;
    size_t offset;
    size_t max_size;
} ConfigParameter;

static ConfigParameter general_params[] =
{
   GENERAL_CONFIG_PARAMETER(app_version, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(signalling_version, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(schema_version, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(server_url, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(server_access_token, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(device_id, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(audio_transmit, CONFIG_TYPE_BOOL),
   GENERAL_CONFIG_PARAMETER(audio_receive, CONFIG_TYPE_BOOL),
   GENERAL_CONFIG_PARAMETER(video_stream_id, CONFIG_TYPE_INT),
   GENERAL_CONFIG_PARAMETER(gst_log_level, CONFIG_TYPE_STRING),
   GENERAL_CONFIG_PARAMETER(adc_partition, CONFIG_TYPE_STRING)
};

static ConfigParameter webrtc_params[] =
{
   WEBRTC_CONFIG_PARAMETER(ice_transport_policy, CONFIG_TYPE_STRING),
   WEBRTC_CONFIG_PARAMETER(bundle_policy, CONFIG_TYPE_STRING),
   WEBRTC_CONFIG_PARAMETER(network_interface, CONFIG_TYPE_STRING)
};

static ConfigParameter soup_params[] =
{
   SOUP_CONFIG_PARAMETER(strict_ssl, CONFIG_TYPE_BOOL),
   SOUP_CONFIG_PARAMETER(logging, CONFIG_TYPE_BOOL)
};

static gboolean s_app_config_schema_version_equal(guint major, guint minor, guint patch)
{
    return (major == APP_CONFIG_SCHEMA_VERSION_MAJOR)
            && (minor == APP_CONFIG_SCHEMA_VERSION_MINOR)
            && (patch == APP_CONFIG_SCHEMA_VERSION_PATCH);
}

static AppConfigResult s_app_config_parse_general_config(AppConfig *config, JsonObject *json_obj)
{
    gint i;
    for (i = 0; i < sizeof(general_params) / sizeof(general_params[0]); ++i) {
        if (json_object_has_member(json_obj, general_params[i].key)) {

            switch (general_params[i].type) {
            case CONFIG_TYPE_STRING:
                g_strlcpy((gchar*) (config) + general_params[i].offset,
                        json_object_get_string_member(json_obj,
                                general_params[i].key),
                        general_params[i].max_size);
                break;
            case CONFIG_TYPE_BOOL:
                *((gboolean*) ((gchar*) (config) + general_params[i].offset)) =
                        json_object_get_boolean_member(json_obj,
                                general_params[i].key);
                break;
            case CONFIG_TYPE_INT:
                *((gint*) ((gchar*) (config) + general_params[i].offset)) =
                        json_object_get_int_member(json_obj,
                                general_params[i].key);
                break;
            case CONFIG_TYPE_DOUBLE:
                *((gdouble*) ((gchar*) (config) + general_params[i].offset)) =
                        json_object_get_double_member(json_obj,
                                general_params[i].key);
                break;
            default:
                return CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE;
            }
        } else {
            return CONFIG_RESULT_ERR_MISSING_FIELD;
        }
    }

    return CONFIG_RESULT_OK;
}

static AppConfigResult s_app_config_parse_webrtc_config(WebrtcConfig *config, JsonObject *json_obj)
{
    gint i;
    for (i = 0; i < sizeof(webrtc_params) / sizeof(webrtc_params[0]); ++i) {
        if (json_object_has_member(json_obj, webrtc_params[i].key)) {
            switch (webrtc_params[i].type) {
            case CONFIG_TYPE_STRING:
                g_strlcpy((gchar*) (config) + webrtc_params[i].offset,
                        json_object_get_string_member(json_obj,
                                webrtc_params[i].key),
                        webrtc_params[i].max_size);
                break;
            case CONFIG_TYPE_BOOL:
                *((gboolean*) ((gchar*) (config) + webrtc_params[i].offset)) =
                        json_object_get_boolean_member(json_obj,
                                webrtc_params[i].key);
                break;
            case CONFIG_TYPE_INT:
                *((gint*) ((gchar*) (config) + webrtc_params[i].offset)) =
                        json_object_get_int_member(json_obj,
                                webrtc_params[i].key);
                break;
            case CONFIG_TYPE_DOUBLE:
                *((gdouble*) ((gchar*) (config) + webrtc_params[i].offset)) =
                        json_object_get_double_member(json_obj,
                                webrtc_params[i].key);
                break;
            default:
                return CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE;
            }
        } else {
            return CONFIG_RESULT_ERR_MISSING_FIELD;
        }
    }

    return CONFIG_RESULT_OK;
}

static AppConfigResult s_app_config_parse_soup_config(SoupConfig *config, JsonObject *json_obj)
{
    gint i;
    for (i = 0; i < sizeof(soup_params) / sizeof(soup_params[0]); ++i) {
        if (json_object_has_member(json_obj, soup_params[i].key)) {
            switch (soup_params[i].type) {
            case CONFIG_TYPE_STRING:
                g_strlcpy((gchar*) (config) + soup_params[i].offset,
                        json_object_get_string_member(json_obj,
                                soup_params[i].key),
                        soup_params[i].max_size);
                break;
            case CONFIG_TYPE_BOOL:
                *((gboolean*) ((gchar*) (config) + soup_params[i].offset)) =
                        json_object_get_boolean_member(json_obj,
                                soup_params[i].key);
                break;
            case CONFIG_TYPE_INT:
                *((gint*) ((gchar*) (config) + soup_params[i].offset)) =
                        json_object_get_int_member(json_obj,
                                soup_params[i].key);
                break;
            case CONFIG_TYPE_DOUBLE:
                *((gdouble*) ((gchar*) (config) + soup_params[i].offset)) =
                        json_object_get_double_member(json_obj,
                                soup_params[i].key);
                break;
            default:
                return CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE;
            }
        } else {
            return CONFIG_RESULT_ERR_MISSING_FIELD;
        }
    }

    return CONFIG_RESULT_OK;
}

static AppConfigResult s_app_config_create_json_general(AppConfig* config, JsonObject** json_obj)
{
    JsonObject* main_obj = json_object_new();
    if (main_obj) {
        gint i;
        for (i = 0; i < sizeof(general_params) / sizeof(general_params[0]); ++i) {
            switch (general_params[i].type) {
            case CONFIG_TYPE_STRING:
                json_object_set_string_member(main_obj, general_params[i].key, (gchar*) (config) + general_params[i].offset);
                break;
            case CONFIG_TYPE_BOOL:
                json_object_set_boolean_member(main_obj, general_params[i].key, *((gboolean*) ((gchar*) (config) + general_params[i].offset)));
                break;
            case CONFIG_TYPE_INT:
                json_object_set_int_member(main_obj, general_params[i].key, *((gint*) ((gchar*) (config) + general_params[i].offset)));
                break;
            case CONFIG_TYPE_DOUBLE:
                json_object_set_double_member(main_obj, general_params[i].key, *((gdouble*) ((gchar*) (config) + general_params[i].offset)));
                break;
            default:
                return CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE;
            }
        }
        *json_obj = main_obj;
    }

    return CONFIG_RESULT_OK;
}

static AppConfigResult s_app_config_create_json_webrtc(WebrtcConfig* config, JsonObject** json_obj)
{
    JsonObject* webrtc_obj = json_object_new();
    if (webrtc_obj) {
        gint i;
        for (i = 0; i < sizeof(webrtc_params) / sizeof(webrtc_params[0]); ++i) {
            switch (webrtc_params[i].type) {
            case CONFIG_TYPE_STRING:
                json_object_set_string_member(webrtc_obj, webrtc_params[i].key, (gchar*) (config) + webrtc_params[i].offset);
                break;
            case CONFIG_TYPE_BOOL:
                json_object_set_boolean_member(webrtc_obj, webrtc_params[i].key, *((gboolean*) ((gchar*) (config) + webrtc_params[i].offset)));
                break;
            case CONFIG_TYPE_INT:
                json_object_set_int_member(webrtc_obj, webrtc_params[i].key, *((gint*) ((gchar*) (config) + webrtc_params[i].offset)));
                break;
            case CONFIG_TYPE_DOUBLE:
                json_object_set_double_member(webrtc_obj, webrtc_params[i].key, *((gdouble*) ((gchar*) (config) + webrtc_params[i].offset)));
                break;
            default:
                return CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE;
            }
        }
        *json_obj = webrtc_obj;
    }

    return CONFIG_RESULT_OK;
}

static AppConfigResult s_app_config_create_json_soup(SoupConfig* config, JsonObject** json_obj)
{
    JsonObject* soup_obj = json_object_new();
    if (soup_obj) {
        gint i;
        for (i = 0; i < sizeof(soup_params) / sizeof(soup_params[0]); ++i) {
            switch (soup_params[i].type) {
            case CONFIG_TYPE_STRING:
                json_object_set_string_member(soup_obj, soup_params[i].key, (gchar*) (config) + soup_params[i].offset);
                break;
            case CONFIG_TYPE_BOOL:
                json_object_set_boolean_member(soup_obj, soup_params[i].key, *((gboolean*) ((gchar*) (config) + soup_params[i].offset)));
                break;
            case CONFIG_TYPE_INT:
                json_object_set_int_member(soup_obj, soup_params[i].key, *((gint*) ((gchar*) (config) + soup_params[i].offset)));
                break;
            case CONFIG_TYPE_DOUBLE:
                json_object_set_double_member(soup_obj, soup_params[i].key, *((gdouble*) ((gchar*) (config) + soup_params[i].offset)));
                break;
            default:
                return CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE;
            }
        }
        *json_obj = soup_obj;
    }

    return CONFIG_RESULT_OK;
}

/*********************************************************************/
/******************************** API ********************************/
/*********************************************************************/

gchar const* app_config_result_to_string(AppConfigResult result)
{
    return  result == CONFIG_RESULT_OK ?                        "OK" :
            result == CONFIG_RESULT_ERR_MEM_ALLOC ?             "ERR_MEM_ALLOC" :
            result == CONFIG_RESULT_ERR_MISSING_FILE ?          "ERR_MISSING_FILE" :
            result == CONFIG_RESULT_ERR_FAILED_TO_PARSE ?       "ERR_FAILED_TO_PARSE" :
            result == CONFIG_RESULT_ERR_BAD_JSON ?              "ERR_BAD_JSON" :
            result == CONFIG_RESULT_ERR_MISSING_FIELD ?         "ERR_MISSING_FIELD" :
            result == CONFIG_RESULT_ERR_UNKNOWN_CONFIG_TYPE ?   "ERR_UNKNOWN_CONFIG_TYPE" :
            result == CONFIG_RESULT_ERR_JSON_OBJECT ?           "ERR_JSON_OBJECT" :
            result == CONFIG_RESULT_ERR_JSON_FILE ?             "ERR_JSON_FILE" :
                                                                "UNKNOWN";
}

void app_config_update(AppConfig* config, CmdArgs const* cmd_args)
{
    if (cmd_args->auth_secret)
        g_strlcpy(config->auth_secret, cmd_args->auth_secret, sizeof(config->auth_secret));

    if (cmd_args->server_url)
        g_strlcpy(config->server_url, cmd_args->server_url, sizeof(config->server_url));

    if (cmd_args->access_token)
        g_strlcpy(config->server_access_token, cmd_args->access_token,
                sizeof(config->server_access_token));

    if (cmd_args->audio_send != -1)
        config->audio_transmit = cmd_args->audio_send;

    if (cmd_args->audio_recv != -1)
        config->audio_receive = cmd_args->audio_recv;

    if (cmd_args->video_stream_id != -1)
        config->video_stream_id = cmd_args->video_stream_id;

    if (cmd_args->verbose)
        config->verbose = cmd_args->verbose;
}

AppConfigResult app_config_parse(AppConfig* config, char const *config_file_path)
{
    GError *error = NULL;
    JsonParser *parser = json_parser_new();
    AppConfigResult ret = CONFIG_RESULT_OK;

    if (access(config_file_path, F_OK) != 0)
        return CONFIG_RESULT_ERR_MISSING_FILE;

    if (!json_parser_load_from_file(parser, config_file_path, &error)) {
        GST_CAT_ERROR(app_config,
                "%s: Failed to parse JSON from file (reason: %s)", G_STRFUNC,
                error->message);
        ret = CONFIG_RESULT_ERR_FAILED_TO_PARSE;
        goto out;
    } else {
        JsonNode *root = NULL;
        root = json_parser_get_root(parser);

        if (!JSON_NODE_HOLDS_OBJECT(root)) {
            ret = CONFIG_RESULT_ERR_BAD_JSON;
            goto out;
        }

        JsonObject* main_object = json_node_get_object(root);
        s_app_config_parse_general_config(config, main_object);

        /* parse webrtc specific configuration */
        if (json_object_has_member(main_object, "webrtc")) {
            JsonObject* webrtc_object = json_object_get_object_member (main_object, "webrtc");
            s_app_config_parse_webrtc_config(&config->webrtc, webrtc_object);

        } else {
            ret = CONFIG_RESULT_ERR_MISSING_FIELD;
            goto out;
        }

        /* parse libsoup specific configuration */
        if (json_object_has_member(main_object, "soup_options")) {
            JsonObject* soup_object = json_object_get_object_member (main_object, "soup_options");
            s_app_config_parse_soup_config(&config->soup, soup_object);
        } else {
            ret = CONFIG_RESULT_ERR_MISSING_FIELD;
            goto out;
        }
    }

out:
    g_object_unref(parser);
    return ret;
}

AppConfigResult app_config_create_file(AppConfig const* config, char const *config_file_path)
{
    JsonNode *root = NULL;
    JsonGenerator *generator = NULL;
    GError *err = NULL;
    AppConfigResult result;

    JsonObject* main_obj = NULL;
    result = s_app_config_create_json_general(config, &main_obj);
    if (result != CONFIG_RESULT_OK || !main_obj) {
        GST_CAT_ERROR(app_config, "main obj create error: %d obj: %p", result, main_obj);
        return CONFIG_RESULT_ERR_JSON_OBJECT;
    }
    JsonObject* webrtc_obj = NULL;
    result = s_app_config_create_json_webrtc(&config->webrtc, &webrtc_obj);
    if (result != CONFIG_RESULT_OK || !webrtc_obj) {
        GST_CAT_ERROR(app_config, "webrtc obj create error: %d obj: %p", result, webrtc_obj);
        return CONFIG_RESULT_ERR_JSON_OBJECT;
    }
    JsonObject* soup_obj = NULL;
    result = s_app_config_create_json_soup(&config->soup, &soup_obj);
    if (result != CONFIG_RESULT_OK || !soup_obj) {
        GST_CAT_ERROR(app_config, "soup obj create error: %d obj: %p", result, soup_obj);
        return CONFIG_RESULT_ERR_JSON_OBJECT;
    }

    json_object_set_object_member(main_obj, "webrtc", webrtc_obj);
    json_object_set_object_member(main_obj, "soup_options", soup_obj);

    root = json_node_init_object(json_node_alloc(), main_obj);
    generator = json_generator_new();
    json_generator_set_root(generator, root);

    json_generator_set_pretty(generator, TRUE);
    if (!json_generator_to_file(generator, config_file_path, &err)) {
        GST_CAT_ERROR(app_config, "failed to generate json file error: %s", err->message);
        g_object_unref (generator);
        json_node_free (root);
        json_object_unref(main_obj);
        return CONFIG_RESULT_ERR_JSON_FILE;
    }

    g_object_unref (generator);
    json_node_free (root);
    json_object_unref(main_obj);

    return CONFIG_RESULT_OK;
}

void app_config_print(AppConfig const* config)
{
    GST_CAT_INFO(app_config, "Configuration");
    GST_CAT_INFO(app_config, "  General:");
    GST_CAT_INFO(app_config, "    app_version: %s", config->app_version);
    GST_CAT_INFO(app_config, "    signalling_version: %s", config->signalling_version);
    GST_CAT_INFO(app_config, "    schema_version: %s", config->schema_version);
    GST_CAT_INFO(app_config, "    server_url: %s", config->server_url);
    GST_CAT_INFO(app_config, "    server_access_token: %s", config->server_access_token);
    GST_CAT_INFO(app_config, "    device_id: %s", config->device_id);
    GST_CAT_INFO(app_config, "    audio_transmit: %d", config->audio_transmit);
    GST_CAT_INFO(app_config, "    audio_receive: %d", config->audio_receive);
    GST_CAT_INFO(app_config, "    video_stream_id: %d", config->video_stream_id);
    GST_CAT_INFO(app_config, "    gst_log_level: %s", config->gst_log_level);
    GST_CAT_INFO(app_config, "    adc_partition: %s", config->adc_partition);
    GST_CAT_INFO(app_config, "  Webrtc:");
    GST_CAT_INFO(app_config, "    ice_transport_policy: %s", config->webrtc.ice_transport_policy);
    GST_CAT_INFO(app_config, "    bundle_policy: %s", config->webrtc.bundle_policy);
    GST_CAT_INFO(app_config, "    network_interface: %s", config->webrtc.network_interface);
    GST_CAT_INFO(app_config, "  Soup Options:");
    GST_CAT_INFO(app_config, "    strict_ssl: %d", config->soup.strict_ssl);
    GST_CAT_INFO(app_config, "    logging: %d", config->soup.logging);
}

void app_config_init(void)
{
    GST_DEBUG_CATEGORY_INIT (app_config, "app_config", 0,
        "app_config class");
}
