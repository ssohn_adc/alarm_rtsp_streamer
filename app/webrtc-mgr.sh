#!/bin/sh
###############################################################################
# @file webrtc-mgr.sh
#
# script for managing alarm-webrtc-streamer
#
# @date 2020-02-26
#
###############################################################################
#
# Alarm.com Copyright 2000-2021
#
# COPYRIGHT STATEMENT.
# This software has been provided pursuant to a License Agreement containing
# restrictions on its use.  The software contains confidential information,
# trade secrets, patents (pending or approved) and proprietary information of
# Alarm.com and is protected by Federal copyright law.  This
# confidential information or source code files, should not be copied or distributed,
# in any form or medium, disclosed to any third party, or taken outside
# Alarm.com or authorized offices or used in any manner, except with prior
# written agreement with Alarm.com . All rights reserved.
#
# Alarm.com
#
###############################################################################
PROG="alarm-webrtc-streamer"
EXEC=/adc/$PROG
EXEC_ALT=/mnt/vendor/$PROG
SCRIPT_NAME=$(basename $0)
PIDFILE=/tmp/$PROG.pid

banner() {
    echo "************************"
    echo "Alarm.com $SCRIPT_NAME"
    echo "************************"
}

info() {
    echo "$(basename $0) [INFO]: $1"
}

warn() {
    echo "$(basename $0) [WARN]: $1"
}

error() {
    echo "$(basename $0) [ERROR]: $1"
    exit 1
}

usage () {
    echo "usage: $SCRIPT_NAME {start|stop|restart|reset}" >&2
    echo "example:"
    echo "  $SCRIPT_NAME start some-secret"
}

process_running() {
    if ps | grep "$PROG" | grep -v "$SCRIPT_NAME" | grep -v grep > /dev/null
    then
        true
    else
        false
    fi
}

start() {
    if [ -f "$EXEC_ALT" ]; then
        info "alternate $PROG exists"
        $EXEC_ALT --auth-secret $1 &
        PID=$!
        echo $PID > $PIDFILE
        info "started alternate $PROG with pid $PID"
    else
        $EXEC --auth-secret $1 &
        PID=$!
        echo $PID > $PIDFILE
        info "started default $PROG with pid $PID"
    fi
}

stop() {
    killall $PROG > /dev/null 2>&1
    PID=$(cat $PIDFILE)
    i=0
    while [ $i -lt 6 ] && process_running
    do
        sleep 1
        i=`expr $i + 1`
    done
    if process_running
    then
        kill -9 $PID
    fi
    rm $PIDFILE > /dev/null 2>&1
    info "$PROG is stopped"
}

reset() {
    if [ -f "$EXEC_ALT" ]; then
        $EXEC_ALT --reset
    else
        $EXEC --reset
    fi
    info "webrtc configuration reset"
}

banner

case "$1" in
    start)
        if process_running
        then
            warn "$PROG is already running with pid $(cat $PIDFILE)"
        else
            if [ -z "$2" ]
            then
                error "auth secret not supplied"
                exit 1
            else
                start $2
            fi
        fi
        ;;

    stop)
        if process_running
        then
            stop
        else
            warn "$PROG is already stopped"
        fi
        ;;

    restart)
        if process_running
        then
            stop
        else
            warn "$PROG is already stopped"
        fi
        if [ -z "$2" ]
        then
            error "auth secret not supplied"
            exit 1
        else
            start $2
        fi
        ;;

    reset)
        stop
        reset
        ;;

    status)
        ;;
    *)
        usage
        exit 1
        ;;
esac

exit 0